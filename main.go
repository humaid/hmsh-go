package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"os/user"
	"regexp"
	"strings"
)

var Host, HomeDir, User string
var HomeDirRegex *regexp.Regexp
var scanner = bufio.NewScanner(os.Stdin)

func getCommandInput() []string {
	var input string
	if scanner.Scan() {
		input = scanner.Text()
	}
	return strings.Split(input, " ")
}

var (
	ANSIReset     = "\033[0m"
	ANSIRedBold   = "\033[31;1m"
	ANSIWhiteBold = "\033[97;1m"
	ANSICyan      = "\033[0;36m"
)

func printPrompt() {
	workDir, err := os.Getwd()
	if err != nil {
		panic("Cannot get current working directory!")
	}
	var prompt rune
	if os.Getuid() == 0 {
		prompt = '#'
	} else {
		prompt = '$'
	}
	truncatedWorkDir := HomeDirRegex.ReplaceAllLiteralString(workDir, "~")

	fmt.Printf("%s%s@%s%s:%s%s %s%c ", ANSIRedBold, User, Host, ANSIWhiteBold,
		ANSICyan, truncatedWorkDir, ANSIReset, prompt)
}

func isBuiltin(command []string) bool {
	if command[0] == "exit" {
		os.Exit(0)
		return true
	} else if command[0] == "cd" {
		if len(command) == 1 {
			os.Chdir(HomeDir)
		} else {
			os.Chdir(command[1])
		}
		return true
	}
	return false
}

func main() {
	HomeDir = os.Getenv("HOME")
	HomeDirRegex = regexp.MustCompile(fmt.Sprintf("^%s", HomeDir))
	user, err := user.Current()
	if err != nil {
		panic("Cannot get current system user")
	}
	User = user.Username
	Host, _ = os.Hostname()
	for {
		printPrompt()
		input := getCommandInput()
		if len(input) > 0 && input[0] != "" {
			var cmd *exec.Cmd
			if len(input) == 1 {
				cmd = exec.Command(input[0])
			} else if len(input) > 1 {
				cmd = exec.Command(input[0], input[1:]...)
			}
			if isBuiltin(input) {
				continue
			}
			cmd.Stdout = os.Stdout
			cmd.Stdin = os.Stdin
			cmd.Stderr = os.Stderr
			if err = cmd.Run(); err != nil {
				fmt.Printf("hmsh: %s\n", err)
			}
		}
	}
}
