# hmsh (Go version)

## 1. Description

hmsh is a very minimal shell, it still doesn't support all built-in shell commands.

This program is rewritten in Go in September 2019, you can still find the old C source at
[git.sr.ht/~humaid/hmsh](https://git.sr.ht/~humaid/hmsh).


## 2. Requirements

The following packages must be installed on your system.

- go

## 3. Copying and contributing

This program is written by Humaid AlQassimi,
and is distributed under the
[BSD 2 Clause](https://humaidq.ae/license/bsd-2-clause) license.  

## 4. Download and compile

```sh
$ git clone git.sr.ht/~humaid/hmsh-go
$ cd hmsh-go
$ go build
```

## 5. Change log

- v0.1 *(Apr 14 2019)*
  - Initial release.
- v0.2 *(Sep 15 2019)*
  - Rewrite in Go.

